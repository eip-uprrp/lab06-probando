#Lab. 6: Pruebas y pruebas unitarias

Como habrás aprendido en experiencias de laboratorio anteriores, el lograr que un programa compile es solo una pequeña parte de programar. El compilador se encargará de decirte si hubo errores de sintaxis, pero no podrea detectar errores en la lógica del programa. Es muy importante el probar las funciones del programa para validar que producen los resultados correctos y esperados.

Una manera de hacer estas pruebas es verificando partes del programa "a mano". Otra manera es insertando pedazos de código o funciones y probando el programa con conjuntos representativos de datos que verifiquen si el programa va haciendo lo que se supone detectar problemas y te ayude a detectar problemas. La biblioteca `cassert` de C++  tiene funciones para hacer pruebas y en la experiencia de laboratorio de hoy utilizaremos una de ellas.



#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán practicado el realizar pruebas a varias versiones de un programa para validar su funcionamiento y el realizar pruebas unitarias utilizando la función `assert` de la biblioteca `cassert`.



#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos básicos relacionados a pruebas y pruebas unitarias.

2. haber repasado el uso de la función `assert` para hacer pruebas.

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 6](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6731) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


#Sesión de laboratorio:

##Ejercicio 1

Este ejercicio es una adaptación del ejercicio en [1]. Se te ha dado un programa que implementa varias versiones de cinco funciones simples, por ejemplo el juego "piedra-papel-tijera" y el ordenar 3 números. Probando las versiones, tú y tu pareja determinarán cuál de las versiones está  implementada correctamente.

Este ejercicion **NO requiere programación**, solo pruebas "a mano".

Las funciones que se implementan en el programa son:

* **3 Sorts:** un programa que recibe tres "strings" y los ordena en orden lexicográfico. Por ejemplo, dados `jirafa`, `zorra`, y `coqui`, los ordena como: `coqui`, `jirafa`, y `zorra`.

* **Dice:** cuando el usuario marca el botón `Roll them`, el programa genera números aleatorios entre 1 y 6. El programa informa la suma de los números aleatorios.

* **Rock, Papers, Scissors:** El usuario entra la jugada para los dos jugadores (`R` para Piedra (Rock), `P` para Papel (Paper) y `S` para Tijeras (Scissors)) y especifica el número de juegos necesarios para ganar el torneo. El programa informa si un jugador gana el torneo.

* **Check Writer:** El usuario entra un número entre 0 y 999,999,999 (incluyéndolo). El resultado del programa es el número escrito en palabras (en inglés). Por ejemplo, si el usuario entra '654123', el programa escribirá 'six hundred fifty four thousand one hundred twenty three'.

* **Zulu time:** Dada una hora en tiempo zulu (Hora en el Meridiano de Greenwich) y la zona militar en la que el usuario desea saber la hora, el programa escribe la hora en esa zona. El formato para el dato de entrada es en formato de 23 horas `####`, por ejemplo `2212` sería las 10:12 pm. La lista de zonas militares válidas la pueden encontrar en  http://en.wikipedia.org/wiki/List_of_military_time_zones. Lo que sigue son ejemplos de cómo deben ser los resultados del programa:

  * Dado 1230 y zona A, el resultado debe ser 1330
  * Dado 1230 y zona N, el resultado debe ser 1130


###**Instrucciones:**

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab06-probando.git` para descargar la carpeta `Lab06-Testing` a tu computadora.

2. Marca doble "click" en el archivo `Testing.pro` para cargar este proyecto a Qt. 

3. Corre el programa. Para cada una de las funciones, prueba todas las versiones y determina cuál es la válida. Para cada función, escribe cuál es la versión correcta en la sección correspondiente de la página de [Entregas del Lab 6](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6732). También indica cuáles fueron las pruebas que realizaste para determinar que las otras versiones son incorrectas.

##Ejercicio 2

Hacer pruebas "a mano" cada vez que corres un programa es una tarea que resulta "cansona" bien rápido. Tú y tu pareja lo hicieron para unas pocas funciones simples. !Imagínate hacer lo mismo para un programa complejo completo como un buscador o un procesador de palabras!

Las pruebas unitarias ayudan a los programadores a validar códigos y simplificar el proceso de depuración ("debugging") a la vez que evitan la tarea tediosa de hacer pruebas a mano en cada ejecución.

###**Instrucciones:**

1. En el menú de QT, ve a `Build` y selecciona `Clean Project "Testing"`.

2. Ve a la carpeta  `Lab06-Testing` y marca doble "click" en el archivo `UnitTests.pro` para cargar este proyecto a Qt. 

3. El proyecto solo contiene el archivo de código fuente `main.cpp`. Este archivo contiene cuatro funciones cuyos resultados son solo parcialmente correctos. Tu tarea es escribir pruebas unitarias para que cada función pueda identificar los resultados erróneos.

  Para la función `fact` se provee a función `test_fact()`  como función de prueba unitaria. Si invocas esta función desde `main`, compilas y corres el programa debes obtener el siguiente mensaje:


  ```cpp
  Assertion failed: (fact(2) == 2), function test_fact, 

  file ../UnitTests/main.cpp, line 69.
  ``` 

  Esto debe ser suficiente para saber que la función `fact` NO está correctamente implementada. Comenta la invocación de `test_fact()` en `main`.


4. Escribe pruebas unitarias para el resto de las funciones. Recuerda que debes llamar a cada una de las funciones de prueba unitaria desde `main` para que corran. Copia cada una de las funciones en la sección correspondiente de la página de [Entregas del Lab 6](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6732).



# Referencias

[1] http://nifty.stanford.edu/2005/TestMe/