#include <iostream>
#include <cassert>

using namespace std;


///
/// \brief fact: Given a positive number will return its factorial.
/// \param n: an integer number
/// \return the factorial
///

unsigned int fact(unsigned int n) {

    if (n <= 0) return 0;

    int result = 1;

    for (int i = 1; i < n; i++)
        result = result * i;

    return result;
}


///
/// \brief isALetter: given a character will determine if its a letter
/// \param c: a character
/// \return true if c is a letter (lower or uppercase), false otherwise
///

bool isALetter(char c) {
    return ( c >= 'A' && c <= 'z');
}


///
/// \brief isValidTime: given the time in military format, determines it is valid
/// \param n: the time in military format, e.g 1835
/// \return true if it is a valid time, false otherwise
///

bool isValidTime(unsigned int n) {
    return ( n >= 0 && n <= 2359 );
}


///
/// \brief gcd: given two positive integers, determines their greatest common divisor
/// \param a: a positive integer
/// \param b: a positive integer
/// \return the greatest common divisor of a and b
///

int gcd ( int a, int b ) {
    int c;

    while ( a > 1 ) {
        c = a;
        a = b % a;
        b = c;
    }
    return b;
}

///
/// \brief test_fact: A unit test function for fact
///

void test_fact() {
    assert( fact(1) == 1 );
    assert( fact(2) == 2);
    assert( fact(4) == 24);
}

int main()
{
    cout << "Go ahead and test!\n";
    test_fact();
    return 0;
}
