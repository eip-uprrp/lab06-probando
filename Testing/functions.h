#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
using namespace std;

bool validateSorts(const QString &a, const QString &b, const QString &c);
void mySortAlpha(QString &a, QString &b, QString &c);
void mySortBeta(QString &a, QString &b, QString &c);
void mySortGamma(QString &a, QString &b, QString &c);
void mySortDelta(QString &a, QString &b, QString &c);

bool validZuluTime(const QString &time, const QString &zone, int &hours, int &minutes);
QString zuluAlpha(int hours, int minutes, char zone);
QString zuluBeta(int hours, int minutes, char zone);
QString zuluGamma(int hours, int minutes, char zone);
QString zuluDelta(int hours, int minutes, char zone);

int RPSAlpha(char p1, char p2, int &score1, int &score2);
int RPSBeta(char p1, char p2, int &score1, int &score2);
int RPSGamma(char p1, char p2, int &score1, int &score2);
int RPSDelta(char p1, char p2, int &score1, int &score2);

void initCheckWMaps();
bool validateCheckQty(QString st, unsigned int &qty);
QString checkWAlpha(int n);
QString checkWBeta(int n);
QString checkWGamma(int n);
QString checkWDelta(int n);

#endif // FUNCTIONS_H
