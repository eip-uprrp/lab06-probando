#ifndef SECONDWINDOW_H
#define SECONDWINDOW_H

#include <QWidget>

class secondwindow : public QWidget
{
    Q_OBJECT
public:
    explicit secondwindow(QWidget *parent = 0);

signals:
    void cerrado(bool si);

protected:
    virtual void closeEvent(QCloseEvent *);

public slots:

};

#endif // SECONDWINDOW_H
