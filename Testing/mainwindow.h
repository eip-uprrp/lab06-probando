#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include "secondwindow.h"

#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QString>
#include <QImage>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void diceAlpha();
    void diceBeta();
    void diceGamma();
    void diceDelta();

    void APFTAlpha();
    void APFTBeta();
    void APFTGamma();
    void APFTDelta();

private slots:
    void on_SortsButton_clicked();
    void on_DiceButton_clicked();
    void on_RPSButton_clicked();
    void on_CheckWriterButton_clicked();
    void on_ZuluButton_clicked();
    void on_APFTButton_clicked();

    void mostrar(bool si);

    void whatOption(QString str);

    void clearLines();

    void RPSnormalizer1(QString str);

    void RPSnormalizer2(QString str);

    void RPSnormalizer3(QString str);

    void CheckWnormalizer(QString str);

    void Zulunormalizer1(QString str);

    void Zulunormalizer2(QString str);

    void APFTnormalizer1(QString str);

    void APFTnormalizer2(QString str);

    void APFTnormalizer3(QString str);

    void sorts();

    void RPSs();

    void dices();

    void checkWs();

    void zulus();

    void APFTs();

private:
    Ui::MainWindow *ui;
    secondwindow *window;
    QGridLayout *layout;
    QPushButton *button[2];
    int buttonSize;
    QLineEdit *line[7];
    int lineSize;
    QLabel *label[7];
    int labelSize;
    QComboBox *method;
    QString option;
    QImage dice1;
    QImage dice2;
    int score1, score2;
};

#endif // MAINWINDOW_H
